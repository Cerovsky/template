package eu.milan.template.splash

import eu.milan.template.core.Architecture
import org.parceler.Parcel

interface SplashContract {

    interface View : Architecture.View

    interface ViewModel : Architecture.ViewModel<View, Model>

    @Parcel
    class Model

}