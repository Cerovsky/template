package eu.milan.template.core


import android.arch.lifecycle.ViewModel
import android.os.Bundle
import android.util.Log
import org.parceler.Parcels

/**
 * Base class for business logic implementation.
 *
 * @param V type of view
 * @param M type of model
 */
abstract class BaseViewModel<V : Architecture.View, M> :
        ViewModel(),
        Architecture.ViewModel<V, M> {

    /**
     * View associated with this view model.
     */
    protected var view: V? = null
        private set

    final override var model: M? = null

    final override fun bindView(view: V) {
        // bind view
        this.view = view
        // notify
        onBindView(view)
    }

    final override fun unbindView() {
        // unbind view
        this.view = null
        // notify
        onUnbindView()
    }

    final override fun restoreState(arguments: Bundle?, savedInstanceState: Bundle?) {
        when {
            savedInstanceState != null -> model = unwrapModel(savedInstanceState)
            arguments != null -> model = unwrapModel(arguments)
            else -> Log.w(javaClass.simpleName, "No saved instance state nor arguments found.")
        }
        onRestoreState(arguments, savedInstanceState)
    }

    final override fun saveState(bundle: Bundle) {
        wrapModel(bundle, model)
        onSaveState(bundle)
    }

    /**
     * Called after state had been restored.
     */
    protected open fun onRestoreState(arguments: Bundle?, savedInstanceState: Bundle?) {}

    /**
     * Called after state had been saved.
     */
    protected open fun onSaveState(bundle: Bundle) {}

    /**
     * Called after view had been bound.
     */
    protected open fun onBindView(view: V) {}

    /**
     * Called after view had been unbound.
     */
    protected open fun onUnbindView() {}

    /**
     * Save model into instance state.
     *
     * @param savedInstanceState saved instance state
     * @param model              model which to save
     */
    protected open fun wrapModel(savedInstanceState: Bundle, model: M?) = model?.takeUnless { it is Unit }?.let { savedInstanceState.putParcelable(Architecture.ARG_MODEL, Parcels.wrap(it)) }

    /**
     * Retrieve model from bundle.
     *
     * @param bundle instance state or arguments
     * @return model
     */
    protected open fun unwrapModel(savedInstanceState: Bundle): M? = Parcels.unwrap(savedInstanceState.getParcelable(Architecture.ARG_MODEL))
}
