package eu.milan.template.core

import android.os.Bundle
import android.support.annotation.LayoutRes
import eu.milan.template.BR

interface Architecture {

    /**
     * Basic interface every fragment & activity should implement.
     */
    interface View

    /**
     * Basic interface every view model has to implement.
     *
     * @param V type of view
     * @param M type of model
     *
     */
    interface ViewModel<in V : View, M> {

        /**
         * Model containing instance state.
         */
        var model: M?

        /**
         * Restore instance state.
         */
        fun restoreState(arguments: Bundle?, savedInstanceState: Bundle?)

        /**
         * Save instance state.
         */
        fun saveState(bundle: Bundle)

        /**
         * Bind view to the view model.
         */
        fun bindView(view: V)

        /**
         * Unbind view from the view model.
         */
        fun unbindView()
    }

    /**
     * Data class containing view config.
     *
     * @property layoutRes  layout resource
     * @property clazz      class of view model
     * @property viewModel  view model resource
     * @property model      model resource
     */
    class ViewConfig<T : BaseViewModel<*, *>>(@LayoutRes val layoutRes: Int, val clazz: Class<T>, val viewModel: Int = BR.VM, val model: Int = BR.M)

    companion object Constants {

        /**
         * Bundle key to store model.
         */
        const val ARG_MODEL: String = "arg_model"
    }
}
