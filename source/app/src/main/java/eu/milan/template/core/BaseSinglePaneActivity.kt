package eu.milan.template.core

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import eu.milan.template.R

/**
 * Base class for activity containing single fragment.
 */
abstract class BaseSinglePaneActivity : AppCompatActivity() {

    private lateinit var pane: FrameLayout

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singlepane)
        pane = findViewById(R.id.pane)

        if (savedInstanceState == null) {
            supportFragmentManager?.beginTransaction()?.add(R.id.pane, createFragment())?.commit()
        }
    }

    protected abstract fun createFragment(): Fragment
}