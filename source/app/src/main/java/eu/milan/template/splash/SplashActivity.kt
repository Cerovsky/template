package eu.milan.template.splash


import android.os.Bundle
import eu.milan.template.R
import eu.milan.template.core.Architecture
import eu.milan.template.core.BaseActivity
import eu.milan.template.databinding.ActivitySplashBinding


class SplashActivity : BaseActivity<SplashViewModel, ActivitySplashBinding>(), SplashContract.View {

    override val viewConfig: Architecture.ViewConfig<SplashViewModel>
        get() = Architecture.ViewConfig(R.layout.activity_splash, SplashViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.bindView(this)
    }
}
