package eu.milan.template.core


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

/**
 * Base class for activity.
 *
 * @param VM    type of view model
 * @param B     type of binding
 */
abstract class BaseActivity<VM : BaseViewModel<*, *>, out B : ViewDataBinding> :
        AppCompatActivity(),
        Architecture.View {

    /**
     * View model associated with this activity.
     */
    protected val viewModel: VM by lazy {
        ViewModelProviders.of(this).get(viewConfig.clazz)
    }

    /**
     * Data binding associated with this activity's view.
     */
    protected val binding: B by lazy {
        DataBindingUtil.setContentView(this, viewConfig.layoutRes) as B
    }

    /**
     * View configuration.
     */
    protected abstract val viewConfig: Architecture.ViewConfig<VM>

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // create view model
        viewModel.restoreState(intent?.extras, savedInstanceState)
        // create binding
        if (!binding.setVariable(viewConfig.model, viewModel.model) || !binding.setVariable(viewConfig.viewModel, viewModel)) {
            throw IllegalStateException()
        }
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        viewModel.saveState(outState as Bundle)
    }

    @CallSuper
    override fun onDestroy() {
        viewModel.unbindView()
        super.onDestroy()
    }

}
