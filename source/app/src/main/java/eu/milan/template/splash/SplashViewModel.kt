package eu.milan.template.splash

import android.os.Bundle
import eu.milan.template.core.BaseViewModel

class SplashViewModel : BaseViewModel<SplashContract.View, SplashContract.Model>(), SplashContract.ViewModel {

    override fun onRestoreState(arguments: Bundle?, savedInstanceState: Bundle?) {
        super.onRestoreState(arguments, savedInstanceState)

        if (model == null) {
            model = SplashContract.Model()
        }
    }
}