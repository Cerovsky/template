package eu.milan.template.core


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Base class for fragment.
 *
 * @param VM    type of view model
 * @param B     type of binding
 */
abstract class BaseFragment<VM : BaseViewModel<*, *>, out B : ViewDataBinding> :
        Fragment(),
        Architecture.View {

    /**
     * View model associated with this fragment.
     */
    protected val viewModel: VM by lazy {
        ViewModelProviders.of(this).get(viewConfig.clazz)
    }

    /**
     * Data binding associated with this fragment's view.
     */
    protected val binding: B by lazy {
        DataBindingUtil.inflate(LayoutInflater.from(activity), viewConfig.layoutRes, null, false) as B
    }

    /**
     * View configuration.
     */
    protected abstract val viewConfig: Architecture.ViewConfig<VM>

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // create view model
        viewModel.restoreState(arguments, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // create binding
        if (!binding.setVariable(viewConfig.model, viewModel.model) || !binding.setVariable(viewConfig.viewModel, viewModel)) {
            throw IllegalStateException()
        }
        return binding.root
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.saveState(outState)
    }

    @CallSuper
    override fun onDestroyView() {
        viewModel.unbindView()
        super.onDestroyView()
    }

    @CallSuper
    override fun onDestroy() {
        viewModel.unbindView()
        super.onDestroy()
    }

}
