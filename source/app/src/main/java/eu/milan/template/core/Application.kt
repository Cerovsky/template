package eu.milan.template.core


import android.os.StrictMode

import eu.milan.template.BuildConfig
import eu.milan.template.FLAVOR_DEVELOPER

class Application : android.app.Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (FLAVOR_DEVELOPER.equals(BuildConfig.FLAVOR)) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())

            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build())
        }
    }

    companion object {
        var instance: Application? = null
            private set
    }

}
