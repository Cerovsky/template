package eu.milan.template.extensions


@Suppress("UNCHECKED_CAST")
fun <T> Any.getPrivateField(fieldName: String): T? {
    return this.javaClass.getDeclaredField(fieldName)
            .also { it.isAccessible = true }
            .let {
                it.get(this) as? T?
            }
}